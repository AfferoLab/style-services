﻿'use strict';

//REQUIRE
const AWS = require('aws-sdk');
const sharp = require("sharp");
const stream = require('stream');

class ImageProcessor {

    constructor(awsCredentials) {
        AWS.config.update(awsCredentials);
    }

    uploadFromStream(s3, destinationBucketName, destinationKey, cb) {
        var pass = new stream.PassThrough();

        var params = { Bucket: destinationBucketName, Key: destinationKey, Body: pass };

        s3.upload(params, function (err, data) {
            if (err) {
                cb(err.message || err);
            }
            else {
                cb();
            }
        });

        return pass;
    }

    deleteSourceObj(s3, sourceBucketName, sourceKey, cb) {
        var params = {
            Bucket: sourceBucketName,
            Key: sourceKey
        };

        s3.deleteObject(params, function (err, data) {
            if (err) {
                cb(err.message || err);
            }
            else {
                cb();
            }
        });
    }

    resizeCompactAndMoveImage(sourceBucketName, sourceKey, destinationBucketName, destinationKey, newWidth, newHeight, keepSource, cb) {

        var s3 = new AWS.S3();

        var s3Stream = s3.getObject({ Bucket: sourceBucketName, Key: sourceKey }).createReadStream();

        // Listen for errors returned by the service
        s3Stream.on('error', function (err) {
            cb(err.message || err)
        });

        var transformer = sharp()
            .resize(newWidth, newHeight, { fit: 'inside' });

        s3Stream.pipe(transformer)
            .on('error', function (err) {
                cb(err.message || err)
            }).pipe(this.uploadFromStream(s3, destinationBucketName, destinationKey, (err) => {
                if (err) {
                    cb(err.message || err);
                }
                else {
                    if (!keepSource) {
                        this.deleteSourceObj(s3, sourceBucketName, sourceKey, cb);
                    }
                    else {
                        cb();
                    }
                }
            }))
            .on('error', function (err) {
                cb(err.message || err);
            });
    }
}

module.exports = ImageProcessor;
