'use strict';

let del = require('del');
let gulp = require('gulp');
let zip = require('gulp-zip');

let releasePath = 'build/distributions/';
let exceptions = ['!style.js', '!gulpfile.js'];

gulp.task('clean', function() {
    return del([releasePath]);
});

gulp.task('build-package', gulp.series('clean', function() { 
    return gulp.src(exceptions.concat(['./**/*.*']))
        .pipe(zip('style.zip'))
        .pipe(gulp.dest(releasePath));
}));

