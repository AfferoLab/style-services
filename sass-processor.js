﻿'use strict';

class SassProcessor {
    constructor(autoprefixer, cleanCss, postcss, request, sass) {
        this.autoprefixer = autoprefixer;
        this.cleanCss = cleanCss;
        this.postcss = postcss;
        this.request = request;
        this.sass = sass;
    }

    downloadRawSassFile(fileAddress, cb) {
        if (!fileAddress) {
            cb(`Raw Sass file address is undefined`);
            return;
        }
        this.request({url: fileAddress, headers: {'User-Agent': 'style-services/x.x request'}}, function (error, response, fileContent) {
            if (response.statusCode === 200) {
                cb(null, fileContent);
            }
            else {
                cb(`Could not fetch file ${fileAddress}.`);
            }
        });
    }

    replacePlaceHolders(rawSass, primaryColor, secondaryColor, cb) {
        let placeholders = [];
        if (primaryColor) {
            placeholders.push(`{{$color-primary-placeholder}}`);
        }
        if (secondaryColor) {
            placeholders.push(`{{$color-secondary-placeholder}}`);
        }
        if (!rawSass ||
            (primaryColor && rawSass.indexOf(placeholders[0]) === -1) ||
            (secondaryColor && rawSass.indexOf(placeholders[1]) === -1)) {
            cb(`Could not find the colors placeholders in the raw sass file.
                Make sure these placeholders '${placeholders.join(', ')}' exist.`);
            return;
        }
        if (primaryColor) {
            rawSass = rawSass.replace(placeholders[0], primaryColor);
        }
        if (secondaryColor) {
            rawSass = rawSass.replace(placeholders[1], secondaryColor);
        }
        cb(null, rawSass);
    }

    compileSass(rawSass, cb) {
        try {
            let css = this.sass.renderSync({ data: rawSass }).css.toString();
            this.postcss([this.autoprefixer]).process(css).then((result) => {
                cb(null, new this.cleanCss().minify(result.css).styles);
            });
        }
        catch (ex) {
            cb(ex.message);
        }
    }

    getCSS(fileAddress, primaryColor, secondaryColor, appKey, cb) {

        this.downloadRawSassFile(fileAddress, (err, rawSass) => {
            if (!err) {
                this.replacePlaceHolders(rawSass, primaryColor, secondaryColor, (err, sassReadyToCompile) => {
                    if (!err) {
                        this.compileSass(sassReadyToCompile, cb);
                        return;
                    }
                    cb(err);
                });
                return;
            }
            cb(err);
        });
    }
}

module.exports = SassProcessor;
