'use strict';

class RedisManager {
    constructor(redis, connectionData) {
        this.connectionError = null;
        this.client = redis.createClient(connectionData);
        this.client.on('error', (err) => {
            this.connectionError = err;
        });
        this.client.on('connect', (err) => {
            this.connectionError = null;
        });
        this.timeout = connectionData.timeout;
    }

    getItem(key, cb) {
        this.client.get(key, cb);
        this.client.expire(key, this.timeout);
    }

    setItem(key, value) {
        this.client.set(key, value);
        this.client.expire(key, this.timeout);
    }
}

module.exports = RedisManager;
