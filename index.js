'use strict';

//REQUIRE
const config = require('./style');
const http = require('http');
const redisMngr = require('./redis-manager');
const sassProc = require('./sass-processor');
const imageProc = require('./image-processor');
const url = require('url');

//CONSTANTS
const redisManager = new redisMngr(require('redis'), config.redis);
const sassProcessor = new sassProc(require('autoprefixer'), require('clean-css'), require('postcss'), require('request'), require('node-sass'));
const imageProcessor = new imageProc(config.aws);

//INIT WEB SERVER
http.createServer(function (req, res) {
    let handleErrorResponse = function (error) {
        console.log("Server error: " + error);
        res.writeHead(500, { 'Content-Type': 'text/html' });
        res.end(error);
    };
    let handleClientErrorResponse = function (error) {
        console.log("Client error: " + error);
        res.writeHead(400, { 'Content-Type': 'text/html' });
        res.end(error);
    };
    let handleOkResponse = function (css, contentType = 'text/css') {
        res.writeHead(200, { 'Content-Type': contentType });
        res.end(css);
    };

    //Check Redis connection
    if (redisManager.connectionError) {
        handleErrorResponse(JSON.stringify(redisManager.connectionError));
        return;
    }

    if (req.url.toLowerCase().indexOf('admin/healthcheck') !== -1) {
        handleOkResponse('');
        return;
    }
    else if (req.url.toLowerCase().indexOf('resize-image') !== -1) {

        console.log('Request for resize-image...');

        if (req.method == 'POST') {

            req.setEncoding('utf8');

            var jsonString = '';

            req.on('data', function (data) {
                jsonString += data;
            });

            req.on('end', function () {
                try {

                    let payload = JSON.parse(jsonString)

                    console.log(payload);

                    if (payload.sourceBucketName && payload.sourceKey && payload.destinationBucketName && payload.destinationKey && payload.newWidth && payload.newHeight) {
                        try {
                            imageProcessor.resizeCompactAndMoveImage(payload.sourceBucketName, payload.sourceKey, payload.destinationBucketName, payload.destinationKey, parseInt(payload.newWidth), parseInt(payload.newHeight), payload.keepSource, (err) => {
                                if (!err) {
                                    console.log("Request for resize-image... SUCCESS!");
                                    handleOkResponse('{}', 'application/json');
                                    return;
                                }

                                handleErrorResponse(err);
                            });
                        }
                        catch (ex) {
                            handleErrorResponse(ex.message);
                        }
                    }
                    else {
                        handleClientErrorResponse(`Invalid arguments. Please provide values for 'sourceBucketName', 'sourceKey', 'destinationBucketName',
                                     'destinationKey', 'newWidth', 'newHeight' and 'quality'.`);
                    }
                }
                catch (ex) {
                    handleClientErrorResponse(ex.message);
                }
            });
        }

        return;
    }
    else {
        //Get params
        let query = url.parse(req.url, true).query;

        let host = req.headers.host;

        let rawScssAddress = '';

        //Handle css requests
        if (((query.color1 && query.appKey) || (query.color1 && query.color2)) && query.version) {

            let key = `${query.color1}_`;
            if (query.color2) {
                key = key + `${query.color2}`;
            }
            if (query.appKey) {
                key = key + `${query.appKey}`;
            }
            key = key + `${query.version}`;


            redisManager.getItem(key, (err, css) => {
                if (css) {
                    handleOkResponse(css);
                    return;
                }
                let orgKey = '';
                if (host.indexOf('afferolab.net') != -1) {
                    orgKey = '/afferolab';
                }

                rawScssAddress = (host.indexOf('localhost') === -1) ? 'https://' + host + orgKey + '/css/raw.scss' : config.scss;

                sassProcessor.getCSS(rawScssAddress, query.color1, query.color2, query.appKey, (err, css) => {
                    if (!err) {
                        redisManager.setItem(key, css);
                        handleOkResponse(css);
                        return;
                    }
                    handleErrorResponse(err);
                });
            });
            return;
        }
        handleErrorResponse(`Invalid arguments. Please provide values for 'color1', 'color2' and 'version' to get a response.`);
    }
}).listen(config.port, config.host);

console.log(`Server running at ${config.host}:${config.port}`);
