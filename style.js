'use strict';

module.exports = {

    "host": process.env.STYLE_SERVICE__HOST || "127.0.0.1",
    "port": process.env.STYLE_SERVICE__PORT || 5000,
    "scss": process.env.STYLE_SERVICE__SCSS || "https://stag-start.afferolab.net/afferolab/css/raw.scss",
    "redis": {
        "host": process.env.STYLE_SERVICE__REDIS_HOST || "127.0.0.1",
        "port": process.env.STYLE_SERVICE__REDIS_PORT || 6379,
        "timeout": process.env.STYLE_SERVICE__REDIS_TIMEOUT || 604800,
    },
    "aws":
    {
        "accessKeyId": process.env.STYLE_SERVICE__AWS_ACCESS_KEY_ID || "",
        "secretAccessKey": process.env.STYLE_SERVICE__AWS_SECRET_ACCESS_KEY || "",
        "region": process.env.STYLE_SERVICE__AWS_REGION || "us-west-2"
    }
};
